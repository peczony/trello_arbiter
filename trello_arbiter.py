#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import functools
import json
import os
import re
import sys
from typing import NamedTuple

import pyexcel
import requests
import toml
from chgksuite.common import DefaultNamespace, get_chgksuite_dir, get_source_dirs
from chgksuite.composer import RedditExporter, parse_4s
from chgksuite.trello import fix_trello_new_editor_links

_open = open
open = functools.partial(_open, encoding="utf8")

API = "https://trello.com/1"
SEPARATOR = "-" * 62
APPEAL_MARK = "Апелляция: "
APPEAL_SEP = "--------------------------------------------------------------"
RE_APPEAL = re.compile("""ID: (?P<id>[0-9]+),Тип: (?P<type>[A-ZА-Я]),Вопрос №(?P<question_number>[0-9]+)(,Ответ: (?P<answer>.+?))?,Статус: (?P<status>[A-ZА-Я]),Апелляция:(?P<appeal_text>.*)""", flags=re.DOTALL)
STUB = """# Текст вопроса

{question}

# Текст апелляции

{appeal}

# Вердикт
"""

dummy = DefaultNamespace()
sourcedir, resourcedir = get_source_dirs()
dummy.labels_file = os.path.join(resourcedir, "labels_ru.toml")
exporter = RedditExporter([], dummy, {})


def reddit_format_question(question):
    return exporter.reddit_format_question(question)


def download_appeals(tournament_id, packet):
    req = requests.get(
        f"https://api.rating.chgk.net/tournaments/{tournament_id}/appeals.json"
    )
    if req.status_code != 200:
        sys.stderr.write("Requests error: {} {}\n".format(req.status_code, req.text))
        sys.exit(1)
    result = []
    appeals = req.json()
    for appeal in appeals:
        item = {
            "appeal_id": appeal["id"],
            "appeal_type": appeal["type"],
            "question_number": appeal["questionNumber"],
            "appeal_text": appeal["appeal"].replace("\r", ""),
            "question_text": packet.get(appeal["questionNumber"], ""),
        }
        if item["appeal_type"] == "A":
            item["appeal_answer"] = appeal["answer"]
        result.append(item)
    return result


def parse_from_text(text_file, packet):
    with open(text_file, encoding="utf8") as f:
        appeals = f.read().split(APPEAL_SEP)
    appeals = [RE_APPEAL.search(a) for a in appeals if RE_APPEAL.search(a)]
    result = []
    for appeal in appeals:
        qn = int(appeal.group("question_number"))
        result.append(
            {
                "appeal_id": int(appeal.group("id")),
                "appeal_type": appeal.group("type"),
                "question_number": qn,
                "appeal_text": appeal.group("appeal_text").replace("\r", "").strip(),
                "appeal_answer": appeal.group("answer"),
                "question_text": packet.get(qn, ""),
            }
        )
    return result


def wrap_packet(packet):
    return {
        int(x[1]["number"]): reddit_format_question(x[1])
        .replace(">!", "")
        .replace("!<", "")
        for x in packet
        if x[0] == "Question"
    }


def format_appeal(appeal):
    header = "Апелляция {aid} (вопрос {qid}, {zachet})".format(
        aid=appeal["appeal_id"],
        qid=appeal["question_number"],
        zachet="снятие"
        if "appeal_answer" not in appeal
        else "зачёт «{}»".format(appeal["appeal_answer"]),
    )
    text = STUB.format(
        question=appeal.get("question_text", ""), appeal=appeal["appeal_text"]
    )
    return {
        "header": header,
        "text": text,
        "appeal_id": appeal["appeal_id"],
        "question_number": appeal["question_number"],
    }


def get_int_from_name(name):
    re_ = re.search("[0-9]+", name)
    if re_:
        return int(re_.group(0))


def load_board(args, trello_json):
    print(f"loading cards from board {args.board_id}...")
    req = requests.get(
        "{}/boards/{}".format(API, args.board_id), params=trello_json["params"]
    )
    assert req.status_code == 200
    board = req.json()
    print("cards loaded!")
    return board


class CardsAndList(NamedTuple):
    list_id: str
    cards: list[dict]


def get_list_cards(args, trello_json):
    board = load_board(args, trello_json)
    open_lists = list(filter(lambda lst: not lst["closed"], board["lists"]))
    target_list_search = [x for x in open_lists if args.list_name in x["name"]]
    target_list_id = target_list_search[0]["id"]
    cards = [c for c in board["cards"] if c["idList"] == target_list_id]
    return CardsAndList(cards=cards, list_id=target_list_id)


def get_sorting(card):
    name = card["name"]
    sp = name.split()
    appeal_id = int(sp[1])
    question_number = int(sp[3].replace(",", ""))
    if "зачёт" in name:
        try:
            part = re.search("«(.+)»", name).group(0)
        except Exception as e:
            print(f"couldn't get answer from {part}: {type(e)} {e}", file=sys.stderr)
            part = "___"
    else:
        part = "___"
    return (question_number, part, appeal_id)


def determine_longest_ordered_sublist(lst):
    if not lst:
        return []
    max_sublist = []
    potential_starts = []
    for i in range(0, len(lst) - 1):
        if len(max_sublist) >= (len(lst) - i):
            break
        if i > 0 and (i not in potential_starts or i in max_sublist):
            continue
        curr_sublist = [i]
        for j in range(i + 1, len(lst)):
            if i == 0 and lst[j] > lst[j - 1]:
                potential_starts.append(j - 1)
            if lst[j] > lst[curr_sublist[-1]]:
                curr_sublist.append(j)
        if len(curr_sublist) > len(max_sublist):
            max_sublist = curr_sublist
    return max_sublist


class Change(NamedTuple):
    card_id: str
    pos: int


def get_pos_changes(cards):
    pos_list = [x["pos"] for x in cards]
    sublist = determine_longest_ordered_sublist(pos_list)
    prev = None
    changes = {}
    prev_pos = None

    for i, card in enumerate(cards):
        if i not in sublist:
            if prev:
                changes[card["id"]] = prev_pos + 1
            else:
                ref_card = cards[sublist[0]]
                new_pos = ref_card["pos"] - (sublist[0] - i)
                if new_pos <= 0:
                    new_pos = 1
                changes[card["id"]] = new_pos
        elif prev and card["pos"] <= prev_pos:
            changes[card["id"]] = prev_pos + 1
        prev = card
        prev_pos = changes.get(prev["id"]) or prev["pos"]
    return [Change(card_id=k, pos=v) for k, v in changes.items()]


def upload_to_trello(args, appeals, trello_json):
    target_list_id, cards = get_list_cards(args, trello_json)
    names = [c["name"] for c in cards]
    existing_ids = [get_int_from_name(n) for n in names if get_int_from_name(n)]
    appeals_ = [a for a in appeals if a["appeal_id"] not in existing_ids]
    if not appeals_:
        print("no new appeals to upload")
    for appeal in appeals_:
        print(f"uploading appeal {appeal['appeal_id']}...")
        requests.post(
            "{}/lists/{}/cards".format(API, target_list_id),
            {
                "key": trello_json["params"]["key"],
                "token": trello_json["params"]["token"],
                "desc": appeal["text"],
                "name": appeal["header"],
            },
        )
    if appeals_:
        print("downloading cards once again to fix sorting")
        target_list_id, cards = get_list_cards(args, trello_json)
    cards = sorted(cards, key=get_sorting)
    for c in cards:
        print(f"name: {c['name']}, pos: {c['pos']}")
    changes = get_pos_changes(cards)
    if not changes:
        print("list is already sorted, no need to sort!")
    for change in changes:
        card_url = f"{API}/cards/{change.card_id}"
        print(f"changing pos for card {change.card_id} to {change.pos}...")
        payload = {
            "key": trello_json["params"]["key"],
            "token": trello_json["params"]["token"],
            "pos": change.pos,
        }
        req = requests.put(card_url, params=payload)
        assert req.status_code == 200


def trello_load():
    token_path = os.path.join(get_chgksuite_dir(), ".trello_token")
    with open(token_path) as f:
        token = f.read().strip()
    with open(os.path.join(resourcedir, "trello.json")) as f:
        trello_json = json.load(f)
    trello_json["params"]["token"] = token
    return trello_json


def parse_appeal_from_card(card):
    result = {}
    parsed_name = re.search("Апелляция ([0-9]+) \\(вопрос ([0-9]+)", card["name"])
    if not parsed_name:
        print("couldn't parse card {}".format(card["name"]))
        return
    result["appeal_id"] = int(parsed_name.group(1))
    result["question_number"] = int(parsed_name.group(2))
    if "зачёт" in card["name"]:
        result["appeal_type"] = "A"
        try:
            result["appeal_answer"] = re.search("зачёт «(.+?)»", card["name"]).group(1)
        except (TypeError, AttributeError):
            print("couldn't parse card {}".format(card["name"]))
            return
    else:
        result["appeal_answer"] = ""
        result["appeal_type"] = "R"
    try:
        result["verdict"] = fix_trello_new_editor_links(
            card["desc"].split("# Вердикт\n")[1].strip()
        )
    except IndexError:
        print("couldn't parse card {}".format(card["name"]))
        return
    result["verdict"] = result["verdict"].replace("\\_", "_")
    if (
        "Вердикт: зачесть" in result["verdict"]
        or "Вердикт: принять" in result["verdict"]
    ):
        result["verdict_short"] = "A"
    elif "Вердикт: отклонить" in result["verdict"]:
        result["verdict_short"] = "D"
    else:
        result["verdict_short"] = ""
    return result


def to_xlsx_row(parsed_card_appeal):
    return [
        parsed_card_appeal["appeal_id"],
        parsed_card_appeal["appeal_type"],
        parsed_card_appeal["question_number"],
        parsed_card_appeal["appeal_answer"],
        parsed_card_appeal["verdict_short"],
        parsed_card_appeal["verdict"],
    ]


def download_verdicts(args, appeals, trello_json):
    board = load_board(args, trello_json)
    appeal_ids = {x["appeal_id"] for x in appeals}
    open_lists = {x["id"] for x in board["lists"] if not x["closed"]}
    cards = [
        c
        for c in board["cards"]
        if get_int_from_name(c["name"]) in appeal_ids
        and not c["closed"]
        and c["idList"] in open_lists
        and "ШАБЛОН" not in c["name"]
    ]
    parsed_appeals = [
        parse_appeal_from_card(card) for card in cards if parse_appeal_from_card(card)
    ]
    header = [" ID", "Тип", "№", "Ответ", "Статус", "Комментарий"]
    to_xlsx = [header] + [to_xlsx_row(appeal) for appeal in parsed_appeals]
    xlsx_fn = "tournament-appeals-{}.xlsx".format(args.tournament_id)
    pyexcel.save_as(dest_file_name=xlsx_fn, array=to_xlsx)
    print(f"verdicts exported to {xlsx_fn}!")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--tournament_id", "-t")
    parser.add_argument("--board_id", "-b")
    parser.add_argument("--config", "-c")
    parser.add_argument("--list_name", "-l")
    parser.add_argument("--debug", action="store_true")
    parser.add_argument("--packet", "-p")
    parser.add_argument("--from_text", "-ft", action="store_true")
    parser.add_argument("--verdicts", "-v", action="store_true")
    args = parser.parse_args()

    if args.config:
        with open(args.config, "r") as f:
            config = toml.loads(f.read())
        for key in config:
            setattr(args, key, config[key])

    assert bool(args.tournament_id)
    assert bool(args.board_id)

    assert bool(args.packet)
    assert bool(args.list_name)

    cwd = os.getcwd()
    os.chdir(os.path.dirname(os.path.abspath(args.packet)))
    with open(args.packet) as f:
        packet = wrap_packet(parse_4s(f.read()))
    os.chdir(cwd)

    appeals = []
    for tournament_id in args.tournament_id.split(","):
        print(f"downloading appeals from {tournament_id}...")
        if args.from_text:
            file_name = f"tournament-appeals-{tournament_id}.txt"
            _appeals = parse_from_text(file_name, packet)
        else:
            _appeals = download_appeals(tournament_id, packet)
        if _appeals:
            print(f"found appeals for tournament {tournament_id}")
        appeals.extend(_appeals)


    formatted_appeals = sorted(
        [format_appeal(a) for a in appeals],
        key=lambda x: (x["question_number"], x["appeal_id"]),
    )

    if args.verdicts and not args.debug:
        trello_json = trello_load()
        download_verdicts(args, appeals, trello_json)
        sys.exit(0)
    if not args.verdicts and not args.debug:
        trello_json = trello_load()
        upload_to_trello(args, formatted_appeals, trello_json)


if __name__ == "__main__":
    main()
