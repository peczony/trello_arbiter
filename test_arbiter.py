import pytest

from trello_arbiter import determine_longest_ordered_sublist, get_pos_changes


def show_vals(lst, sublist):
    return [lst[i] for i in sublist]


SUBLIST_TEST_CASES = [
    ([1000, 999, 1, 2, 3, 5, 4, 6, 7, -123], [1, 2, 3, 5, 6, 7]),
    ([1000, 999, 1, 2, 3, 5, 4, 6, 7, -123, 8, 7, 12], [1, 2, 3, 5, 6, 7, 8, 12]),
    ([1000, 999, 1, -2, 3, 4, 6], [-2, 3, 4, 6]),
    ([10, 9, 8, 7, 6, 5, 4, 3, 2, 1], [10]),
    ([], []),
    ([1, 2, 3, 4], [1, 2, 3, 4]),
    ([1, 2, 3, 2, 4, 7], [1, 2, 3, 4, 7]),
]


@pytest.mark.parametrize("lst,sublist", SUBLIST_TEST_CASES)
def test_longest_sublist(lst, sublist):
    tested_sublist = determine_longest_ordered_sublist(lst)
    assert show_vals(lst, tested_sublist) == sublist


CHANGES_TEST_CASES = [
    ([], []),
    ([1, 2, 2, 3, 5], [1, 2, 3, 4, 5]),
    ([101, 100, 98, 2, 3, 4], [1, 2, 3, 4, 5, 6]),
]


@pytest.mark.parametrize("before_changes,after_changes", CHANGES_TEST_CASES)
def test_changes(before_changes, after_changes):
    cards = [{"id": str(i), "pos": x} for i, x in enumerate(before_changes)]
    changes = {x.card_id: x.pos for x in get_pos_changes(cards)}
    for c in cards:
        if c["id"] in changes:
            c["pos"] = changes[c["id"]]
    tested_after_changes = [x["pos"] for x in cards]
    assert tested_after_changes == after_changes
